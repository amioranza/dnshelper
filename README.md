# dnshelper
Simple GO DNS that returns the IP in URL to make local development simple

Deeply inspired by xip.io and nip.io but written in go and now it just answer non-authoritative type A queries.

The idea is to use it with your platform that can create automatically urls like my.app.12.34.56.78.domain.local where my.app is the route to your app, the 12.34.56.78 is the IP of the machine hosting the app and the ip returned by the DNS, and the last part is any domain that you want to use to serve the app, it can be a internal server with conditional forwarder to it or a external DNS.

# Run via docker:

## Linux instructions:

1. Add a second IP on your interface or use some loopback, you will run two dns server on the same machine, the port 53/udp need to be open to the DNS service.

```
sudo ip a add dev <INTERFACE> IP.OF.YOUR.NETWORK/MASK
```
Validate the new IP added:

```
ip a
```
The return should be something like:

```
3: wlp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether aa:bb:cc:dd:ee:ff brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.14/24 brd 192.168.0.255 scope global dynamic wlp2s0
       valid_lft 2036sec preferred_lft 2036sec
    inet 192.168.0.153/24 scope global secondary wlp2s0
       valid_lft forever preferred_lft forever
```

2. Start a local DNS resolver (dnsmasq):

```
docker run -p <FIRST.IP>:53:53/tcp -p <FIRST.IP>:53:53/udp --cap-add=NET_ADMIN andyshinn/dnsmasq:2.78 -S /<DOMAIN-CHOOSED>/SECOND-IP-ON-YOUR-MACHINE --log-facility=-
```

3. Start dnshelper:

```
docker run -d -p <SECOND.IP>:53:53/udp amioranza/dnshelper:v1 -d <DOMAIN-CHOOSED>
```

*If no domain specified the default domain will be used (domain.tld).*

# Run via docker-compose

1. Customize the docker-compose.yml file included in the repository and run the compose:

```
docker-compose up -d
```

# Run the binary

1. To run the binary just run it with desired parameters:

```
dnshelper -b <IP.TO.BIND> -d any.desired.domain
```

**NOTE:** To use it effectivelly only use with domains that you have control over, and this is only for dev/testing stages. In prod you need to create something more stable and clean.