FROM ubuntu
LABEL maintainer="A. Mioranza <amioranza @ mdcnet.ninja>"
LABEL description="DNS Helper"
WORKDIR /
ADD dnshelper /dnshelper
EXPOSE 53/udp
ENTRYPOINT [ "/dnshelper" ]
