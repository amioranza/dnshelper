package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"regexp"
	"strconv"

	"github.com/miekg/dns"
	flag "github.com/spf13/pflag"
)

const defDomain = "domain.tld."

var author = `A. Mioranza <amioranza @ mdcnet.ninja>`
var (
	bind    string
	help    bool
	mDomain string
)

func extractIP(address string, domain string) (resp net.IP) {
	re := regexp.MustCompile(`(?m)\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}` + domain)
	reIP := regexp.MustCompile(`(?m)(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}`)
	surIP := ""
	ip := ""

	for i, match := range re.FindAllString(address, -1) {
		log.Println(match, "found at index", i)
		surIP = match
	}

	for i, match := range reIP.FindAllString(surIP, -1) {
		log.Println(match, "found at index", i)
		ip = match
	}
	log.Println("Returned IP:", net.ParseIP(ip))
	return net.ParseIP(ip)
}

type handler struct{}

// ServeDNS answers the DNS requests
func (this *handler) ServeDNS(w dns.ResponseWriter, r *dns.Msg) {
	msg := dns.Msg{}
	msg.SetReply(r)
	switch r.Question[0].Qtype {
	case dns.TypeA:
		msg.Authoritative = true
		domain := msg.Question[0].Name
		log.Println("Queried domain:", domain)
		ip := extractIP(domain, mDomain)
		log.Println("Found IP:", ip)
		if ip != nil {
			msg.Answer = append(msg.Answer, &dns.A{
				Hdr: dns.RR_Header{Name: domain, Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: 60},
				A:   ip,
			})
		}
	}
	w.WriteMsg(&msg)
}

func init() {
	flag.StringVarP(&mDomain, "mdomain", "d", "", "Domain to use as suffix")
	flag.StringVarP(&bind, "bind", "b", "", "IP to bind the DNS")
	flag.BoolVarP(&help, "help", "h", false, "Print help message")
}

func printUsage() {
	fmt.Printf("Usage: %s [options]\n", os.Args[0])
	fmt.Println("Options:")
	flag.PrintDefaults()
	os.Exit(1)
}

func main() {
	// parse flags
	flag.Parse()
	if help {
		printUsage()
	}

	log.Println("DNS Helper by", author)

	if mDomain == "" {
		mDomain = defDomain
	}
	log.Println("Default domain:", mDomain)

	if bind != "" {
		if parse := net.ParseIP(bind); parse == nil {
			log.Println("IP to bind:", bind)
			panic("Cannot parse IP to bind")
		}
		log.Println("IP to bind:", bind)
		srv := &dns.Server{Addr: bind + ":" + strconv.Itoa(53), Net: "udp"}
		srv.Handler = &handler{}
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("Failed to set udp listener %s\n", err.Error())
		}
	} else {
		srv := &dns.Server{Addr: ":" + strconv.Itoa(53), Net: "udp"}
		srv.Handler = &handler{}
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("Failed to set udp listener %s\n", err.Error())
		}
	}

}
